package kz.astana.activityintentapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Hello", "(d) onCreate");
        setContentView(R.layout.activity_main);

        Button first = findViewById(R.id.first);
        Button second = findViewById(R.id.second);
        Button third = findViewById(R.id.third);

        first.setOnClickListener(this);
        second.setOnClickListener(this);
        third.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.first) {
            Intent intent = new Intent(MainActivity.this, FirstActivity.class);
            intent.putExtra("TEXT_EXTRA", "Hello from Main");
            startActivity(intent);
        } else if (v.getId() == R.id.second) {
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            intent.putExtra("MESSAGE_EXTRA", "Hello from Main");
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (v.getId() == R.id.third) {
            String address = "http://google.com";
            Uri uri = Uri.parse(address);
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(uri);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Hello", "(e) onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.w("Hello", "(w) onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Hello", "(i) onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("Hello", "(v) onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.wtf("Hello", "(wtf) onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "(d) onDestroy");
    }
}